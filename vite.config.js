import { fileURLToPath, URL } from 'url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  // it is not in vue.config.js publicPath !!!
  base: process.env.NODE_ENV === 'production'
    ? '/realtime-election-predictions-design/'
    : '/'
})
